import type { News } from "@/models/blogs/News";
import { HttpService } from "./HttpService";

export const NewsService = {
  getNews: async () =>
    await HttpService.tch.get("news/newsfeed").then(({ data }) => {
      // console.log(data);
      return data.news as News[];
    }),
};
