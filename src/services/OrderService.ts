import type { Menu } from "./../models/order/Menu";
import { HttpService } from "./HttpService";

export const OrderService = {
  getMenu: async () =>
    await HttpService.tch.post("menu", { src: "TCH-WEB" }).then(({ data }) => {
      // console.log(data);
      return data.menu as Menu[];
      //return data.menu.slice(0, -1) as Menu[];
    }),
};
