export interface Posts {
  button: string;
  content: string;
  deeplink: string;
  html: string;
  id: string;
  publish: number;
  share_url: string;
  thumbnail: string;
  title: string;
  url: string;
}
