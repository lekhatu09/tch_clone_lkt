import type { Posts } from "./Posts";

export interface News {
  name: string;
  posts: Posts[];
}
