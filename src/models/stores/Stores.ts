export interface Stores {
  full_address: string;
  id: number;
  image: string[];
  location: {
    lat: string;
    long: string;
  };
  name: string;
  open_time: string;
  phone: number;
  pu_city: string;
}
